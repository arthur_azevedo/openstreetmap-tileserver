#! /bin/bash

docker_files="-f docker-compose.yml -f docker-compose.servers.yml"
docker-compose ${docker_files} up -d --build