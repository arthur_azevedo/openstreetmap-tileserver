# OpenStreetMap Tileserver with Graphhopper

Projeto de configuração e construção de um servidor de mapas e rotas próprio.

### Ferramentas necessárias

É preciso ter instalado na máquina o [Docker](https://www.docker.com/) e o [Docker-compose](https://docs.docker.com/compose/).

### Instalação

Para poder usar este servidor próprio, primeiro é necessário baixar os arquivos com os dados OSM do mapa e criar o banco de dados.
Foi criado o script init-db para isso, que foi configurado para baixar os dados a partir do servidor [Geofabrik.de](https://download.geofabrik.de/).


```bash
./init.db.sh
```


Também existe a possibilidade de parametrizar a região a ser baixada pelo script através do parâmetro **-r**. O parâmetro padrão está apontado para a região sudeste do Brasil. Exemplo de uso:


```bash
./init.db.sh -r south-america/brazil/norte
```


Após o término da configuração bem sucedida do banco de dados, os serviços do servidor de mapas e de cálculo de rotas poderão ser inicializados pelo script init-server.sh.


```bash
./init.db.sh
```


As portas padrão para acessar os serviços são (* elas podem ser modificadas no arquivo **docker-compose.servers.yml** *):

* Mapa: **8080**
* Rotas: **8989**

