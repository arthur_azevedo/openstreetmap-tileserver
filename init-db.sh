#!/bin/bash

# * PARÂMETROS DOS ARQUIVOS DOCKER
docker_files="-f docker-compose.yml -f docker-compose.db.yml"

# * LIMPANDO POSSÍVEIS CONTAINERS E DADOS ANTERIORES
docker-compose ${docker_files} down
docker rm -f osm-db
docker volume rm openstreetmap-data
rm -rf data/*

# * PARÂMETRO PADRÃO DE REGIÃO
download_region=south-america/brazil/sudeste

while getopts r: flag
do
    case "${flag}" in
        r) download_region=${OPTARG};;
    esac
done
download_url=https://download.geofabrik.de/${download_region}

# * EFETUAR DOWNLOAD DO PBF
wget -O data/data.osm.pbf ${download_url}-latest.osm.pbf
wget -O data/data.poly  ${download_url}.poly

# * SUBIR CONTAINER DO BANCO, QUE IRÁ CRIAR O BANCO.
docker-compose ${docker_files} up -d --build