// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  MAP_SERVER_URL: 'http://localhost:8080',
  ROUTE_API_URL: 'http://localhost:8989',
  LOCATION_API_URL: 'http://localhost:8181'
};
