import { Component, OnInit } from '@angular/core';
import * as olProj from 'ol/proj';
import { MapService } from 'src/app/services/map.service';
import { RouteService } from 'src/app/services/route.service';


@Component({
  selector: 'main-map',
  templateUrl: './main-map.component.html',
  styleUrls: ['./main-map.component.scss']
})
export class MainMapComponent implements OnInit {

  private readonly baseCenter = olProj.fromLonLat([-43.9542, -19.8157]);
  private readonly defaultZoomLevel = 6;

  constructor(
    private mapService: MapService
  ) { }

  ngOnInit(): void {
    this.initMap();
    this.setMapExtent();
  }

  /**
   * * Inicializando o mapa.
   */
  private initMap(): void {
    this.mapService.initMap('mainMap', this.baseCenter, this.defaultZoomLevel);
  }

  setMapExtent(): void {
    const mapExtent = this.mapService.mapExtent;
    if (mapExtent) {
      this.mapService.updateExtent(
        mapExtent, this.baseCenter,
        this.defaultZoomLevel
      );
    }
  }

}
