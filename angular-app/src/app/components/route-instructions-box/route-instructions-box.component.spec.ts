import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RouteInstructionsBoxComponent } from './route-instructions-box.component';

describe('RouteInstructionsBoxComponent', () => {
  let component: RouteInstructionsBoxComponent;
  let fixture: ComponentFixture<RouteInstructionsBoxComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RouteInstructionsBoxComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RouteInstructionsBoxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
