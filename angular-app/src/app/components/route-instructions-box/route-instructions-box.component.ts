import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { RouteInstruction } from 'src/app/model/route-response.model';
import { RouteService } from 'src/app/services/route.service';

@Component({
  selector: 'route-instructions-box',
  templateUrl: './route-instructions-box.component.html',
  styleUrls: ['./route-instructions-box.component.scss']
})
export class RouteInstructionsBoxComponent implements OnInit {

  constructor(private routeService: RouteService) { }

  ngOnInit(): void {
  }

  get instructions(): Observable<RouteInstruction[]> {
    return this.routeService.routeInstructions;
  }

  get hasInstructions(): Observable<boolean> {
    return this.instructions.pipe(map(i => i && i.length > 0));
  }

}
