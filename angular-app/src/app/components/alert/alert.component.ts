import { Component, OnInit } from '@angular/core';
import { debounceTime, map } from 'rxjs/operators';
import { Alert } from '../../model/alert.model';
import { MessageService } from '../../services/message.service';

@Component({
  selector: 'alert',
  templateUrl: './alert.component.html',
  styleUrls: ['./alert.component.scss']
})
export class AlertComponent implements OnInit {

  constructor(private alertService: MessageService) { }

  ngOnInit(){
    this.alertService.alert.pipe(
      debounceTime(5000),
      map(alerts => alerts.filter(a => a.autoClose))
    ).subscribe(
      alerts => alerts.forEach(a => this.removeAlert(a))
    );
  }

  removeAlert(alert: Alert) {
    this.alertService.dismiss(alert);
  }

  get alerts() {
    return this.alertService.alert;
  }

  get hasAlerts() {
    return this.alerts.pipe(
      map(alerts => alerts && alerts.length > 0)
    )
  }

}
