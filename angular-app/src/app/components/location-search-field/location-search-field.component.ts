import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Observable, of, OperatorFunction } from 'rxjs';
import { catchError, debounceTime, distinctUntilChanged, switchMap, tap } from 'rxjs/operators';
import { LocationResponse } from 'src/app/model/location-response.model';
import { LocationSearchService } from 'src/app/services/location-search.service';

@Component({
  selector: 'location-search-field',
  templateUrl: './location-search-field.component.html',
  styleUrls: ['./location-search-field.component.scss']
})
export class LocationSearchFieldComponent implements OnInit {

  model: any;
  searching = false;
  searchFailed = false;

  @Output()
  onchange: EventEmitter<LocationResponse> = new EventEmitter();

  @Output()
  onclear: EventEmitter<LocationResponse> = new EventEmitter();

  constructor(private _service: LocationSearchService) { }

  ngOnInit(): void {
  }

  search: OperatorFunction<string, LocationResponse[]> = (text$: Observable<string>) =>
    text$.pipe(
      debounceTime(300),
      distinctUntilChanged(),
      tap(() => this.searching = true),
      switchMap(term =>
        this._service.search(term).pipe(
          tap(() => this.searchFailed = false),
          catchError(() => {
            this.searchFailed = true;
            return of([]);
          }))
      ),
      tap(() => this.searching = false)
    );

  formatter = (model: LocationResponse) => model.display_name;

  onChange(value: LocationResponse | undefined) : void {
    if (value) {
      this.onchange.emit(value);
    }
  }

  clear(): void {
    let oldModel = this.model;
    this.model = null;
    this.onclear.emit(oldModel);
  }

}
