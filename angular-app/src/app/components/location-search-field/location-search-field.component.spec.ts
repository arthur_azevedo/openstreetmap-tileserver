import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LocationSearchFieldComponent } from './location-search-field.component';

describe('LocationSearchFieldComponent', () => {
  let component: LocationSearchFieldComponent;
  let fixture: ComponentFixture<LocationSearchFieldComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LocationSearchFieldComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LocationSearchFieldComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
