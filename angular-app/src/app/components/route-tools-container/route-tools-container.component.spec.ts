import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RouteToolsContainerComponent } from './route-tools-container.component';

describe('RouteToolsContainerComponent', () => {
  let component: RouteToolsContainerComponent;
  let fixture: ComponentFixture<RouteToolsContainerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RouteToolsContainerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RouteToolsContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
