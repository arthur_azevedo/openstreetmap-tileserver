import { Component, OnInit } from '@angular/core';
import { RouteService } from 'src/app/services/route.service';

@Component({
  selector: 'route-tools-container',
  templateUrl: './route-tools-container.component.html',
  styleUrls: ['./route-tools-container.component.scss'],
  viewProviders: [RouteService]
})
export class RouteToolsContainerComponent implements OnInit {

  constructor(private routeService: RouteService) { }

  ngOnInit(): void {
    this.routeService.initRouteClick();
  }

}
