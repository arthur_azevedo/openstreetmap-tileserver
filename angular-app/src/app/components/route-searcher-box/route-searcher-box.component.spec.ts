import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RouteSearcherBoxComponent } from './route-searcher-box.component';

describe('RouteSearcherBoxComponent', () => {
  let component: RouteSearcherBoxComponent;
  let fixture: ComponentFixture<RouteSearcherBoxComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RouteSearcherBoxComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RouteSearcherBoxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
