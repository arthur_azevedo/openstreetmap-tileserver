import { AfterViewInit, Component, OnInit, QueryList, ViewChildren } from '@angular/core';
import { LocationResponse } from 'src/app/model/location-response.model';
import { RouteService } from 'src/app/services/route.service';
import { LocationSearchFieldComponent } from '../location-search-field/location-search-field.component';

@Component({
  selector: 'route-searcher-box',
  templateUrl: './route-searcher-box.component.html',
  styleUrls: ['./route-searcher-box.component.scss']
})
export class RouteSearcherBoxComponent implements OnInit, AfterViewInit {

  private _routeBoxes = 2;

  @ViewChildren(LocationSearchFieldComponent)
  private fields!: QueryList<LocationSearchFieldComponent>;

  constructor(
    private routeService: RouteService
  ) { }

  ngOnInit(): void {
    
  }

  ngAfterViewInit() {
    console.log(this.fields);
  }

  get routeBoxes() {
    return new Array(this._routeBoxes);
  }

  onChangeLocation(location: LocationResponse) {
    const lat = location.lat, lon = location.lon;
    if (lat && lon) {
      const newFeature = this.routeService.addPoint([lon, lat]);
      location.feature = newFeature;
    }
  }

  onClearLocation(location: LocationResponse) {
    const feature = location.feature;
    if (feature) {
      this.routeService.removePointFeature(feature);  
    }
  }

  addNewLocation = () => this._routeBoxes++;
  
  clearAll() {
    this._routeBoxes = 2;
    this.routeService.clearAll();
    this.fields.forEach(field => field.model = null);
  }

}
