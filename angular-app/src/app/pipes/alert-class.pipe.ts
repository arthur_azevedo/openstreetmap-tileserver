import { Pipe, PipeTransform } from '@angular/core';
import { Alert, AlertType } from '../model/alert.model';

@Pipe({
  name: 'alertClass'
})
export class AlertClassPipe implements PipeTransform {

  transform(value: Alert, ...args: unknown[]): string {
    const type = args[0];
    if (type === 'icon') {
      return this.getIcon(value);
    } else {
      return this.getClass(value);
    }
  }

  private getIcon(value: Alert): string {
    switch (value.type) {
      case AlertType.Success:
            return 'fa-check-circle';
      case AlertType.Error:
          return 'fa-times-circle';
      case AlertType.Info:
          return 'fa-info-circle';
      case AlertType.Warning:
          return 'fa-exclamation-triangle';
    }
  }

  private getClass(value: Alert): string {
    switch (value.type) {
      case AlertType.Success:
          return 'success';
      case AlertType.Error:
          return 'error';
      case AlertType.Info:
          return 'info';
      case AlertType.Warning:
          return 'warning';
  }
  }

}
