import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'routeTime'
})
export class RouteTimePipe implements PipeTransform {

  transform(value: number, ...args: unknown[]): string | null {
    if (value <= 0) {
      return null;
    }
    // Em Minutos
    const duracao = value / 60000;
    const durUnidade = "min";
    let durStr = duracao.toLocaleString('pt-BR', {
      minimumFractionDigits: 0,
      maximumFractionDigits: 2
    });
    return `${durStr} ${durUnidade}`;
  }

}
