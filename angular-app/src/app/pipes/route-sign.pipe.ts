import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'routeSign'
})
export class RouteSignPipe implements PipeTransform {

  private readonly PATH: string = '/assets/img';

  transform(value: number, ...args: unknown[]): string | null {
    switch(value) {
      case 0:
        return this.resolvePath('continue');
      case -2:
        return this.resolvePath('left');
      case 2:
        return this.resolvePath('right');
      case 1:
        return this.resolvePath('slight_right');
      case -1:
        return this.resolvePath('slight_left');
      case 7:
        return this.resolvePath('keep_right');
      case -7:
        return this.resolvePath('keep_left');
      case 3:
        return this.resolvePath('sharp_right');
      case -3:
        return this.resolvePath('sharp_left');
      case -98:
      case 6:
        return this.resolvePath('roundabout');
      case 4:
        return this.resolvePath('marker-to');
      case 5:
        return this.resolvePath('marker-small-green')
      default:
        return this.resolvePath('unknown');      
    }
  }

  private resolvePath(imgName: string) {
    return `${this.PATH}/${imgName}.png`;
  }

}
