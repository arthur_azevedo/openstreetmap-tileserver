import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'distance'
})
export class DistancePipe implements PipeTransform {

  transform(value: number, ...args: unknown[]): string | null {
    const distance = value && value > 0 ? (value / 1000).toLocaleString('pt-BR', {
      minimumFractionDigits: 0,
      maximumFractionDigits: 2
    }) : '0';
    return `${distance} Km`;
  }

}
