import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { AppComponent } from './app.component';
import { NgbModule, NgbTooltipModule, NgbTypeaheadModule } from '@ng-bootstrap/ng-bootstrap';
import { MainMapComponent } from './components/main-map/main-map.component';
import { RouteSearcherBoxComponent } from './components/route-searcher-box/route-searcher-box.component';
import { RouteInstructionsBoxComponent } from './components/route-instructions-box/route-instructions-box.component';
import { MapService } from './services/map.service';
import { AlertComponent } from './components/alert/alert.component';
import { MessageService } from './services/message.service';
import { CommonModule } from '@angular/common';
import { DistancePipe } from './pipes/distance.pipe';
import { RouteTimePipe } from './pipes/route-time.pipe';
import { RouteToolsContainerComponent } from './components/route-tools-container/route-tools-container.component';
import { RouteSignPipe } from './pipes/route-sign.pipe';
import { AlertClassPipe } from './pipes/alert-class.pipe';
import { LocationSearchFieldComponent } from './components/location-search-field/location-search-field.component';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    MainMapComponent,
    RouteSearcherBoxComponent,
    RouteInstructionsBoxComponent,
    AlertComponent,
    DistancePipe,
    RouteTimePipe,
    RouteToolsContainerComponent,
    RouteSignPipe,
    AlertClassPipe,
    LocationSearchFieldComponent
  ],
  imports: [
    BrowserModule,
    NgbModule,
    HttpClientModule,
    CommonModule,
    NgbTypeaheadModule,
    FormsModule,
    NgbTooltipModule
  ],
  providers: [
    MapService,
    MessageService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
