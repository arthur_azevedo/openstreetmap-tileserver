export class RouteResponse {
    constructor(
        public paths: RoutePath[]
    ) {}
}

export class RoutePath {
    constructor(
        public bbox: number[],
        public distance: number,
        public instructions: RouteInstruction[],
        public points: any[],
        public time: number
    ) {}
}

export class RouteInstruction {
    constructor(
        public distance: number,
        public heading: number,
        public interval: number[],
        public sign: number,
        public street_name: string,
        public text: string,
        public time: number
    ) {}
}