import { Feature } from "ol";
import Point from "ol/geom/Point";

export class LocationResponse {
    constructor(
        public display_name: string,
        public boundingbox?: number[],
        public lat?: number,
        public lon?: number,
        public place_rank?: number,
        public importance?: number,
        public feature?: Feature<Point>
    ) {}
}