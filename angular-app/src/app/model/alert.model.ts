export class Alert {
  constructor (
      public type: AlertType,
      public title: string,
      public message: string,
      public autoClose: boolean
  ) {}
}

export enum AlertType {
  Success,
  Error,
  Info,
  Warning
}

