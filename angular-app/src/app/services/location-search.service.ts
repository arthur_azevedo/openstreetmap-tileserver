import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { LocationResponse } from '../model/location-response.model';

@Injectable({
  providedIn: 'root'
})
export class LocationSearchService {

  private readonly PATH = environment.LOCATION_API_URL;

  constructor(private http: HttpClient) { }

  public search(queryTerm: string): Observable<LocationResponse[]> {
    const params = new HttpParams().set('q', queryTerm);
    return this.http.get<LocationResponse[]>(this.PATH, {params: params})
      // Ordenando pela importância do resultado...
      .pipe(
        map(locations => locations.sort((l1, l2) => this.orderBy(l1, l2)))
      );
  }

  public reverse(lat: number, lon: number): Observable<LocationResponse[]> {
    const params = new HttpParams().set('lat', lat).set('lon', lon);
    return this.http.get<LocationResponse[]>(this.PATH, {params: params})
      // Ordenando pela importância do resultado...
      .pipe(
        map(locations => locations.sort((l1, l2) => this.orderBy(l1, l2)))
      );
  }

  private orderBy = (l1: LocationResponse, l2: LocationResponse) => l1.importance && l2.importance ? 
    l1.importance - l2.importance : 0; 

}
