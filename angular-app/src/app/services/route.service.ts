import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, defer, Observable } from 'rxjs';
import { MapService } from './map.service';
import { MessageService } from './message.service';
import VectorLayer from 'ol/layer/Vector';
import VectorSource from 'ol/source/Vector';
import { Feature, MapBrowserEvent } from 'ol';
import Point from 'ol/geom/Point';
import GeoJSON from 'ol/format/GeoJSON';
import { transform, fromLonLat, transformExtent } from 'ol/proj';
import { Fill, Icon, Stroke, Style } from 'ol/style';
import { environment } from 'src/environments/environment';
import { RouteInstruction, RouteResponse } from '../model/route-response.model';
import Geometry from 'ol/geom/Geometry';

/**
 *
 * TODO 1: Verificar como parametrizar: Rota como endereço / Rota por evento de clique e suas complicações.
 */

@Injectable({
  providedIn: 'root',
})
export class RouteService {

  private clickLayer: VectorLayer<VectorSource<Point>> | undefined;
  private routeLayer: VectorLayer<VectorSource<Geometry>> | undefined;

  private readonly PATH = environment.ROUTE_API_URL + '/route';

  private _routeInstructions: BehaviorSubject<RouteInstruction[]> = 
    new BehaviorSubject<RouteInstruction[]>([]);

  constructor(
    private http: HttpClient,
    private messageService: MessageService,
    private mapService: MapService
  ) {}

  private get map() {
    return this.mapService.map;
  }

  private get clickLayerSource() {
    return this.clickLayer?.getSource();
  }

  private get routeLayerSource() {
    return this.routeLayer?.getSource();
  }

  get routeInstructions() {
    return this._routeInstructions.asObservable();
  }

  initRouteClick() {
    this.createClickLayer();
    this.createRouteLayer();
  }

  private createClickLayer(): void {
    this.clickLayer = new VectorLayer({
      source: new VectorSource({ features: [] }),
    });

    const map = this.map;
    if (map) {
      map.addLayer(this.clickLayer);
      map.on('click', (event) => this.onClickLayerEvent(event));
    }

    const source = this.clickLayerSource;
    if (source) {
      source.on('addfeature', () => this.recalculateRoute());
      source.on('removefeature', () => this.recalculateRoute());
    }
  }

  private createRouteLayer(): void {
    this.routeLayer = new VectorLayer({
      source: new VectorSource({
        features: [],
      }),
    });
    this.map?.addLayer(this.routeLayer);
  }

  private onClickLayerEvent(event: MapBrowserEvent<any>) {
    const source = this.clickLayerSource;
    if (source) {
      const coordinate = transform(
        event.coordinate,
        'EPSG:3857',
        'EPSG:4326'
      );

      this.addPoint(coordinate);
      /**
       * TODO: Ao Adicionar um ponto pelo clique, pesquisar uma localização pelo lat/lon (busca reversa) 
       * e atualizar as caixas de pesquisa com os valores.
       */
      this.recalculateRoute();
    }
  }

  addPoint(coordinates: number[]): Feature<Point> {
    const feature = new Feature({
      geometry: new Point(fromLonLat(coordinates)),
    });
    this.clickLayerSource?.addFeature(feature);
    this.updateIconsStyle();
    return feature;
  }

  removePointFeature(pointFeature: Feature<Point>): void {
    this.clickLayerSource?.removeFeature(pointFeature);
  }

  recalculateRoute() {
    const source = this.clickLayerSource;
    if (source && source.getFeatures().length > 1) {
      const alert = this.messageService.showInfo('Calculando rota...', 'Informação', false);
      this.calculateRoute()
        .toPromise()
        .then((response) => this.drawRoute(response))
        .catch((error) => {
          console.log(error);
          this.clearAll();
          this.messageService.showError('Erro encontrado ao calcular rota.');
        })
        .finally(() => this.messageService.dismiss(alert));
    } else {
      this._routeInstructions.next([]);
      this.routeLayerSource?.clear();
    }
  }

  private updateIconsStyle(): void {
    const source = this.clickLayerSource;
    source?.getFeatures().forEach((feature, index, array) => {
      let pin = 'green';
      if (index === 0) {
        pin = 'blue';
      } else if (index === array.length - 1) {
        pin = 'red'
      }
      const style = new Style({
        image: new Icon({
          anchor: [0.5, 46],
          scale: 0.5,
          anchorXUnits: 'fraction', anchorYUnits: 'pixels',
          src: `assets/img/marker-icon-${pin}.png`,
        })
      });
      feature.setStyle(style);
    });
  }

  private getRouteStyle() {
    return new Style({
      fill: new Fill({
        color: 'rgba(255, 255, 255, 0.6)',
      }),
      stroke: new Stroke({
        color: 'green',
        width: 2,
      }),
    });
  }

  private calculateRoute(): Observable<any> {
    const source = this.clickLayerSource as VectorSource<Point>;
    const points = source
      .getFeatures()
      .map((feature) =>
        this.transformPoint(feature.getGeometry()?.getCoordinates())
      );

    return this.doRequest({ points: points });
  }

  private transformPoint(coordinates: number[] | undefined): number[] | null {
    return coordinates
      ? transform(coordinates, 'EPSG:3857', 'EPSG:4326')
      : null;
  }

  private doRequest(data: any): Observable<RouteResponse> {
    let url = this.PATH;

    url = url
      .concat(
        '?weighting=fastest&elevation=false&use_miles=false&layer=OpenStreetMap&points_encoded=false'
      )
      .concat('&locale=pt-BR&vehicle=car');

    const chDisableStr = '&ch.disable=true';
    const points: number[][] = data.points,
      alternatives: boolean = data.alternatives,
      blockedAreas: any[] = data.blockedAreas;

    if (points?.length > 0) {
      points.forEach((point) => {
        url = url.concat('&point=');
        url = url + point[1] + ',' + point[0];
      });
    }

    if (alternatives) {
      url = url
        .concat(
          '&algorithm=alternative_route&alternative_route.max_share_factor=1&max_paths=2'
        )
        .concat(chDisableStr);
    }

    if (blockedAreas?.length > 0) {
      if (url.indexOf(chDisableStr) === -1) {
        url = url.concat(chDisableStr);
      }
      url = url.concat('&block_area=');
      blockedAreas.forEach((area, i) => {
        url = url + area.latitude + ',' + area.longitude + ',' + area.raio;
        if (i < blockedAreas.length - 1) {
          url += ';';
        }
      });
    }
    return this.http.get<RouteResponse>(url);
  }

  private drawRoute(response: RouteResponse): void {
    const source = this.routeLayerSource;
    this._routeInstructions.next([]);
    if (source && response.paths.length > 0) {
      // Limpando rotas anteriores...
      source.clear();
      response.paths.forEach((path) => {
        const feature = new GeoJSON().readFeature(
          path.points, { featureProjection: 'EPSG:3857' });
        feature.setProperties({
          instructions: path.instructions,
          distance: path.distance,  time: path.time
        });
        feature.setStyle(this.getRouteStyle());
        source.addFeature(feature);
        let bbox = transformExtent(path.bbox, 'EPSG:4326', 'EPSG:3857');
        this.map?.getView().fit(bbox, {padding: [100, 100, 100, 100]});
        this._routeInstructions.next(path.instructions);
      });
      
    }
  }

  clearAll() {
    this.clickLayerSource?.clear();
    this.routeLayerSource?.clear();
    this._routeInstructions.next([]);
  }
}
