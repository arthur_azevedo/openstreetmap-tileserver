import { Injectable } from '@angular/core';
import Map from 'ol/Map';
import View from 'ol/View';
import TileLayer from 'ol/layer/Tile';
import XYZ from 'ol/source/XYZ';
import 'ol/ol.css';

import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class MapService {

  private readonly sourceUrl = environment.MAP_SERVER_URL.concat('/tile/{z}/{x}/{y}.png');
  private mapInstance: Map | null = null;

  constructor() { }

  initMap(target: string, baseCenter: number[], zoom: number): void {
    if (!this.mapInstance) {
      this.mapInstance = new Map({
        target: target,
        layers: [
          new TileLayer({
            source: new XYZ({ url: this.sourceUrl })
          })
        ],
        view: new View({
          center: baseCenter,
          zoom: zoom
        })
      });
    }
    
  }

  get map(): Map | null {
    return this.mapInstance;
  }

  get mapExtent(): number[] | undefined{
    return this.mapInstance?.getView().calculateExtent();
  }

  updateExtent(mapExtent: number[], baseCenter: number[], zoomLevel: number): void {
    console.log(mapExtent, baseCenter);
    this.map?.setView(
      new View({ 
        extent: mapExtent, zoom: zoomLevel, center: baseCenter
      })
    )
  }
}
