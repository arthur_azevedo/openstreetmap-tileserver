import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Alert, AlertType } from '../model/alert.model';

@Injectable()
export class MessageService {

  private readonly messages:
    BehaviorSubject<Alert[]> = new BehaviorSubject<Alert[]>([]);

  constructor() {}

  private showMessage(messageType: AlertType, message: string, title: string, autoClose: boolean): Alert {
    const messages = this.messages.value;
    const alert = new Alert(messageType, title, message, autoClose);
    messages.push(alert)
    this.messages.next(messages);
    return alert;
  }

  showError(message: string, action = 'Falha', autoClose = true): Alert {
    return this.showMessage(AlertType.Error, message, action, autoClose);
  }

  showWarning(message: string, action = 'Atenção', autoClose = true): Alert {
    return this.showMessage(AlertType.Warning, message, action, autoClose);
  }

  showInfo(message: string, action = 'Aviso', autoClose = true): Alert {
    return this.showMessage(AlertType.Info, message, action, autoClose);
  }

  showSuccess(message: string, action = 'Sucesso', autoClose = true): Alert {
    return this.showMessage(AlertType.Success, message, action, autoClose);
  }

  get alert() {
    return this.messages;
  }

  clear(): void {
    this.messages.next([]);
  }

  dismiss(alert: Alert): void {
    let messages = this.messages.value;
    const message = messages.findIndex(a => 
      a.type === alert.type 
      && a.message === a.message 
      && a.title === a.title
    );
    if (message > -1) {
      messages.splice(message, 1);
      this.messages.next(messages);
    }
  }


}
